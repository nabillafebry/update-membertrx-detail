# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/update-membertrx-detail.jar /update-membertrx-detail.jar
# run application with this command line[
CMD ["java", "-jar", "/update-membertrx-detail.jar"]
