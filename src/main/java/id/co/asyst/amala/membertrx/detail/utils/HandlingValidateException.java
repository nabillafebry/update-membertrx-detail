package id.co.asyst.amala.membertrx.detail.utils;

public class HandlingValidateException extends Exception {

    public HandlingValidateException(String message) {
        super(message);
    }
}
