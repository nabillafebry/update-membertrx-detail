package id.co.asyst.amala.membertrx.detail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"membertrx-detail.xml", "beans.xml"})
public class MemberTrxDetailApp {

    public static void main(String[] args) {
        SpringApplication.run(id.co.asyst.amala.membertrx.detail.MemberTrxDetailApp.class, args);
    }
}