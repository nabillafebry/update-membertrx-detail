package id.co.asyst.amala.membertrx.detail.utils;

import id.co.asyst.commons.core.utils.GeneratorUtils;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Utils {
    static Logger LOG = LoggerFactory.getLogger(Utils.class);

    public void generateVar(Exchange exchange) {
        Map<String, Object> source = (Map<String, Object>) exchange.getProperty("source");
        String detailid = (String) source.get("detailid");
        String certificateid = (String) source.get("certificateid");
        int miles = Integer.parseInt((String) source.get("miles"));

        exchange.setProperty("detailid", detailid);
        exchange.setProperty("certificateid", certificateid);
        exchange.setProperty("miles", miles);
    }

    public void countMiles(Exchange exchange) {
        List<Map<String, Object>> accdetail = (List<Map<String, Object>>) exchange.getProperty("accdetail");

        int milesdata = (int) exchange.getProperty("miles");
        int milesacc = Integer.parseInt(accdetail.get(0).get("awardmiles").toString());

        int newmiles = milesacc + milesdata;

        exchange.setProperty("newmiles", newmiles);
    }

    public void generateID(Exchange exchange) {
        Date date = new Date();
        String trxdetailid = GeneratorUtils.GenerateId("", date, 6);

        exchange.setProperty("trxdetailid", trxdetailid);
    }
}
